# Attachment Extractor
|    Metrics    |                                                                                     Master                                                                                     |                                                                                  Develop                                                                                 |
|:-------------:|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|
| CI status     | [![pipeline status](https://gitlab.com/cabildo-tf/vente/attachment-extractor/badges/master/pipeline.svg)](https://gitlab.com/cabildo-tf/vente/attachment-extractor/-/commits/master) | [![pipeline status](https://gitlab.com/cabildo-tf/vente/attachment-extractor/badges/dev/pipeline.svg)](https://gitlab.com/cabildo-tf/vente/attachment-extractor/-/commits/dev) |
| Test coverage | [![coverage report](https://gitlab.com/cabildo-tf/vente/attachment-extractor/badges/master/coverage.svg)](https://gitlab.com/cabildo-tf/vente/attachment-extractor/-/commits/master) | [![coverage report](https://gitlab.com/cabildo-tf/vente/attachment-extractor/badges/dev/coverage.svg)](https://gitlab.com/cabildo-tf/vente/attachment-extractor/-/commits/dev) |


# Descripción
Herramienta que permite realizar operaciones sobre adjuntos de una base de datos gestionada por ArcGIS. 
Entre las operaciones posibles está:
* Extraer una copia del adjunto almacenado a un fichero
* Reducir la resolución del adjunto si es una imágen 
* Modifica campos de la tabla de adjuntos
* Eliminar el adjunto

## Configuración de la base de datos

Define la conexión a la base de datos, la lista de parámetros corresponde con los expuestos por la función 
[arcpy.management.CreateDatabaseConnection](https://pro.arcgis.com/en/pro-app/latest/tool-reference/data-management/create-database-connection.htm).

```yaml
out_folder_path: C:\target
out_name: nombre_db.sde
database_platform: POSTGRESQL
instance: host
account_authentication: DATABASE_AUTH
username: usuario_db
password: password_db
save_user_pass: SAVE_USERNAME
database: name_db

```
Lo ideal es usar un usuario de base de datos que solo tenga acceso a las tablas a las que se vayan a acceder.

```sql
Poner ejemplo
```

## Definición de tareas

Para la exportación de los adjuntos se usar la siguiente configuración:

```yaml
tasks:
  - table: vente.vente.documento
    fieldnames: ["nombre_fichero", "url"]
    query: URL IS NULL OR URL LIKE 'C:%'
    mimetypes:
      '*':
        - job: export
          filename: '{globalid}'
          path: //mediastorage/documents
        - job: update_field
          field: nombre_fichero
          value: '{att_name}'
        - job: update_field
          field: url_base
          value: 'https://example.org/mediastorage/documents/{att_name}'
        - job: delete_attachment
```

| Campo      | Descripción                                                                                          | Valor por defecto |
|------------|------------------------------------------------------------------------------------------------------|-------------------|
| table      | Nombre de la tabla                                                                                   |                   |
| fieldnames | Listado de campos a devolver en la consulta a la base de datos                                       |                   |
| query      | Query en SQL para restringir los resultados                                                          |                   |
| mimetypes  | Tipo de fichero (mimetype) al que se le aplican las tareas definidas. '*' cualquier tipo de fichero. |                   |


### Tareas
Existen 4 tipos de tareas:
* export. Crea una copia en fichero del adjunto almacenado
* update_field. Actualiza el valor de un campo
* delete_attachment. Borra el adjunto de la base de datos
* low_resolution. Reduce la resolución de los adjuntos en caso de ser una imagen

