import setuptools

setuptools.setup(
    name="attach_extractor",
    version="0.0.6",
    author="Ignacio Lorenzo",
    author_email="nacholore@gmail.com",
    description="Extract attachments of ArcGIS Database",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
    install_requires=[
          'pyyaml', 'pillow'
      ],
    entry_points={
        'console_scripts': ['attachment-extractor=attach_extractor.extractor:main'],
    }
)
