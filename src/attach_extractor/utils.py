import os
import yaml
from string import Formatter


class Convert(object):
    def __init__(self, keys):
        self.__fieldnames = keys

    def convert_to_dictionary(self, values):
        feature = {}
        for (key, value) in zip(self.__fieldnames, values):
            feature[key] = value
        return feature

    def convert_to_array(self, values):
        if values:
            row = []
            for key in self.__fieldnames:
                row.append(values[key])
            return row

    def get_fieldnames(self):
        return self.__fieldnames


def load_config(path):
    if path and os.path.exists(path):
        with open(path, 'r') as f:
            return yaml.safe_load(f.read())


def get_keys_from_format_string(text):
    return [fname for _, fname, _, _ in Formatter().parse(text) if fname]


def validate_format_string(fieldnames, field):
    keys_used = get_keys_from_format_string(field)
    if not all(k in fieldnames for k in keys_used):
        raise Exception("No existe alguno de los campos {} en la tabla".format(', '.join(keys_used)))


def generate_class_name(workspace, item):
    table = item['table']
    table_attach = table + '__attach'

    if 'table_attach' in item:
        table_attach = item['table_attach']

    feature_class = workspace + '/' + table
    attach_class = workspace + '/' + table_attach

    return feature_class, attach_class


def process_fieldnames(feature_fieldnames):
    feature_fieldnames.append('globalid') if 'globalid' not in feature_fieldnames else feature_fieldnames

    return feature_fieldnames
