import tempfile
import os
import io
import logging
import mimetypes

from PIL import Image
from io import BytesIO
from attach_extractor.utils import validate_format_string
import re

mimetypes.init()


class JobAbstract(object):
    def __init__(self, *args, **kwargs): pass

    def run(self, feature_class, attach_class):
        feature_class, attach_class = self.process(feature_class, attach_class)
        return feature_class, attach_class

    def validate(self, feature_fieldnames, attach_fieldnames):
        pass

    def process(self, feature_class, attach_class):
        pass


class LowResolutionJob(JobAbstract):
    def __init__(self, *args, **kwargs):
        max_size = kwargs.pop('max_size', 256)
        self.__max_size = (max_size, max_size)
        self.__quality = kwargs.pop('quality', 90)
        self.__optimize = kwargs.pop('optimize', True)
        self.__blob_fieldname = kwargs.pop('blob_fieldname', 'data')
        super().__init__(*args, **kwargs)

    def process(self, feature_class, attach_class):
        image_data = attach_class[self.__blob_fieldname]
        attach_class[self.__blob_fieldname] = memoryview(self.optimize_image(image_data))

        return feature_class, attach_class

    def optimize_image(self, image_data):
        image = Image.open(io.BytesIO(image_data.tobytes()))
        format = 'JPEG' if image.mode not in ("RGBA", "P") else 'PNG'
        image.thumbnail(self.__max_size, Image.ANTIALIAS)
        img = BytesIO()
        image.save(img, optimize=True, quality=self.__quality, format=format)

        return img.getvalue()


class ExportAttachmentJob(JobAbstract):
    def __init__(self, *args, **kwargs):
        self.__attachment_folder = kwargs.pop('path')
        self.__filename = kwargs.pop('filename')
        self.__content_type_fieldname = kwargs.pop('content_type', 'content_type')
        self.__att_name_fieldname = kwargs.pop('att_name', 'att_name')
        self.__blob_fieldname = kwargs.pop('blob_fieldname', 'data')

        super().__init__(*args, **kwargs)

    def make_folders(self, folder):
        try:
            os.makedirs(folder, exist_ok=True)
        except FileNotFoundError:
            raise Exception("No exists folder {folder}")

    def validate(self, feature_fieldnames, attach_fieldnames):
        validate_format_string(feature_fieldnames, self.__filename)

    def process(self, feature_class, attach_class):
        attach_class = self.generate_filename(feature_class, attach_class)
        self.export(attach_class)
        return feature_class, attach_class

    def generate_filename(self, feature_class, attach_class):
        name = self.__filename.format(**feature_class)
        name = re.sub(r'{|}', '', name)
        mimetype = attach_class[self.__content_type_fieldname]
        extension = mimetypes.guess_extension(mimetype)
        extension = ".jpg" if extension == ".jpe" else extension
        if not extension:
            extension = ''
        attach_class[self.__att_name_fieldname] = f"{name}{extension}"

        return attach_class

    def export(self, attach_class):
        filename = attach_class[self.__att_name_fieldname]
        data = attach_class[self.__blob_fieldname]
        path = os.path.join(self.__attachment_folder, filename)
        os.makedirs(self.__attachment_folder, exist_ok=True)
        with open(path, 'wb') as f:
            f.write(data.tobytes())


class UpdateFieldJob(JobAbstract):
    def __init__(self, *args, **kwargs):
        self.__field = kwargs.pop('field')
        self.__value = kwargs.pop('value')
        super().__init__(*args, **kwargs)

    def validate(self, feature_fieldnames, attach_fieldnames):
        validate_format_string(attach_fieldnames, self.__value)

    def process(self, feature_class, attach_class):
        try:
            feature_class[self.__field] = self.__value.format(**attach_class)
        except KeyError as e:
            raise Exception("No existe alguno de los campos '{0}'".format(self.__value))
        return feature_class, attach_class


class DeleteAttachmentJob(JobAbstract):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def process(self, feature_class, attach_class):
        return feature_class, None


class JobFactory(object):
    @classmethod
    def create_extractor(cls, config_job):
        name_job = config_job['job']
        if name_job == 'low_resolution':
            return LowResolutionJob(**config_job)
        elif name_job == 'export':
            return ExportAttachmentJob(**config_job)
        elif name_job == 'update_field':
            return UpdateFieldJob(**config_job)
        elif name_job == 'delete_attachment':
            return DeleteAttachmentJob()
        else:
            raise Exception("Attachment type '{0}' is not recognized".format(name_job))
