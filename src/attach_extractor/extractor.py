import logging
import argparse
import os
import arcpy
from attach_extractor.logging.SetupLogging import SetupLogging
from attach_extractor.utils import load_config, generate_class_name, process_fieldnames
from attach_extractor.jobpipeline import JobPipeline


ATTACH_FIELDNAMES = ['attachmentid', 'rel_globalid', 'content_type', 'att_name', 'data_size', 'data', 'globalid']


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--db-config", dest="db_config_path", default="./config/dbconfig.yaml")
    parser.add_argument("-t", "--tasks-config", dest="tasks_config_path", default="./config/tasks.yaml")
    parser.add_argument("-m", "--metrics-config", dest="metrics_config_path")
    parser.add_argument("-l", "--logging-config", dest="logging_config_path", default="./config/logging.yaml")
    args = parser.parse_args()

    SetupLogging(logging_path=args.logging_config_path).setup_logging()

    logging.info("Starting")

    logging.info("Loading configuration")
    db_config = load_config(args.db_config_path)
    tasks_config = load_config(args.tasks_config_path)
    metrics_config = load_config(args.metrics_config_path)
    logging.info("Configuration loaded")

    logging.info("Creating database connection")
    workspace = db_connection(db_config)
    logging.info("Database connection created")

    for item in tasks_config['tasks']:
        logging.info("Exporting items from '{table}'".format(**item))
        num_items = extract(workspace=workspace, item=item)
        logging.info("All items exported")

    logging.info("Completed")


def db_connection(config):
    if all(k in config for k in ('out_folder_path', 'out_name')):
        if not os.path.isfile(os.path.join(config['out_folder_path'], config['out_name'])):
            arcpy.CreateDatabaseConnection_management(**config)
    return config['out_folder_path'] + "/" + config['out_name']


def extract(workspace, item):
    feature_fieldnames = process_fieldnames(item['fieldnames'])
    feature_class, attach_class = generate_class_name(workspace=workspace, item=item)
    query = item['query'] if 'query' in item else None

    job_pipeline = JobPipeline(item['mimetypes'], feature_fieldnames=feature_fieldnames,
                               attach_fieldnames=ATTACH_FIELDNAMES)

    edit = arcpy.da.Editor(workspace)
    edit.startEditing(False, multiuser_mode=True)

    try:
        size = get_size(feature_class, feature_fieldnames, query)
        logging.info("Size: {0}".format(size))

        with arcpy.da.UpdateCursor(feature_class, feature_fieldnames, where_clause=query) as feature_cursor:
            for idx, feature in enumerate(feature_cursor, start=1):
                global_id = feature[feature_fieldnames.index('globalid')]
                attach_query = "REL_GLOBALID = '{0}'".format(global_id)

                logging.info("{0}/{1} Processing item '{2}' in table '{3}'".format(idx, size, global_id, feature_class))

                with arcpy.da.UpdateCursor(attach_class, ATTACH_FIELDNAMES, where_clause=attach_query) as attach_cursor:
                    for attach in attach_cursor:
                        edit.startOperation()
                        feature, attach = job_pipeline.run(feature, attach)
                        if attach:
                            attach_cursor.updateRow(attach)
                        else:
                            attach_class.deleteRow()
                        feature_cursor.updateRow(feature)
                        edit.stopOperation()

    except TypeError as e:
        logging.error("TypeError", e)
        edit.abortOperation()
    except RuntimeError as e:
        logging.error("RunTime", e)
        edit.abortOperation()
    finally:
        edit.stopEditing(True)
    return size


def get_size(feature_class, feature_fieldnames, query):
    rows = [row for row in arcpy.da.SearchCursor(feature_class, feature_fieldnames, where_clause=query)]
    return len(rows)


if __name__ == '__main__':
    main()
