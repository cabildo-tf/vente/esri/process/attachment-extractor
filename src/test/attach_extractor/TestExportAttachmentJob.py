import unittest
import io
from nose.tools import eq_, ok_
from os import path
from PIL import Image, ImageChops
from attach_extractor.job import ExportAttachmentJob


def load_binary(path):
    with open(path, "rb") as image:
        f = image.read()
        return memoryview(bytearray(f))


def compare_image(img1, img2):
    image_1 = Image.open(io.BytesIO(img1.tobytes()))
    image_2 = Image.open(io.BytesIO(img2.tobytes()))

    diff = ImageChops.difference(image_1, image_2)
    return diff.getbbox()


class TestExportAttachmentJobJob(unittest.TestCase):
    def setUp(self):
        self.feature = {
            'globalid': '{010375CD-1497-4C07-A71D-4DBCF185696F}'
        }
        self.attach = {
            'rel_globaid': '{4EA25197-A13C-4B87-B145-9C4C2C31F6D8}',
            'content_type': 'image/jpeg',
            'data': load_binary('test/resources/test.jpg'),
            'att_name': 'test.jpg'
        }

    def test_exportedImageFile_when_exportImage(self):
        config_job = {
            'path': 'temp',
            'filename': '{globalid}'
        }

        job = ExportAttachmentJob(**config_job)
        feature, attach = job.run(self.feature.copy(), self.attach.copy())

        eq_(feature, self.feature)
        eq_(attach['data'], self.attach['data'])
        eq_(attach['att_name'], '010375CD-1497-4C07-A71D-4DBCF185696F.jpg')
        ok_(path.isfile(path.join(config_job['path'], '010375CD-1497-4C07-A71D-4DBCF185696F.jpg')))

    def test_exportedExcelFile_when_exportExcel(self):
        attach_origin = {
            'rel_globaid': '{4EA25197-A13C-4B87-B145-9C4C2C31F6D8}',
            'content_type': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'data': load_binary('test/resources/test.xlsx'),
            'att_name': 'test.xlsx'
        }

        config_job = {
            'path': 'temp',
            'filename': '{globalid}'
        }

        job = ExportAttachmentJob(**config_job)
        feature, attach = job.run(self.feature.copy(), attach_origin.copy())

        eq_(feature, self.feature)
        eq_(attach['data'], attach['data'])
        eq_(attach['att_name'], '010375CD-1497-4C07-A71D-4DBCF185696F.xlsx')
        ok_(path.isfile(path.join(config_job['path'], '010375CD-1497-4C07-A71D-4DBCF185696F.xlsx')))

    def test_returnException_when_fieldnameNotExists(self):
        config_job = {
            'path': '../temp',
            'filename': '{globalid2}'
        }

        job = ExportAttachmentJob(**config_job)

        self.assertRaises(Exception, job.validate, list(self.feature.keys()), list(self.attach.keys()))
