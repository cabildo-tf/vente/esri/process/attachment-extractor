import unittest
import io
from nose.tools import eq_, ok_
from PIL import Image, ImageChops
from attach_extractor.job import LowResolutionJob


def load_image(path):
    with open(path, "rb") as image:
        f = image.read()
        return memoryview(bytearray(f))


def compare_image(img1, img2):
    image_1 = Image.open(io.BytesIO(img1.tobytes()))
    image_2 = Image.open(io.BytesIO(img2.tobytes()))

    return ImageChops.difference(image_1, image_2).getbbox() is None


class TestLowResolutionJob(unittest.TestCase):
    def setUp(self):
        self.feature = {}
        self.attach = {
            'rel_globaid': '{4EA25197-A13C-4B87-B145-9C4C2C31F6D8}',
            'data': load_image('test/resources/test.jpg'),
            'att_name': 'test.jpg'
        }

    def test_returnImageInLowResolution_when_passHDImagen(self):
        job = LowResolutionJob()
        feature, attach = job.run(self.feature.copy(), self.attach.copy())

        eq_(feature, self.feature)
        ok_(not compare_image(attach['data'], self.attach['data']))
