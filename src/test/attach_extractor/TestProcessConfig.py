import unittest
from attach_extractor.utils import generate_class_name
from nose.tools import eq_, ok_


class TestExportAttachmentJobJob(unittest.TestCase):
    def test_returnFeatureAndAttachClasses_when_passTable(self):
        item = {
            "table": "imagen"
        }

        workspace = 'db.sde'

        feature_class, attach_class = generate_class_name(workspace=workspace, item=item)

        eq_(feature_class, workspace + '/' + item['table'])
        eq_(attach_class, workspace + '/' + item['table'] + '__attach')

    def test_returnFeatureAndAttachClasses_when_passTableAndAttach(self):
        item = {
            "table": "a209",
            "table_attach": "a210"
        }

        workspace = 'db.sde'

        feature_class, attach_class = generate_class_name(workspace=workspace, item=item)

        eq_(feature_class, workspace + '/' + item['table'])
        eq_(attach_class, workspace + '/' + item['table_attach'])
