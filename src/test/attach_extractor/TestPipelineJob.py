import unittest
from attach_extractor.utils import load_config
from attach_extractor.jobpipeline import JobPipeline
from nose.tools import eq_, ok_
from os import path


def load_image(path):
    with open(path, "rb") as image:
        f = image.read()
        return memoryview(bytearray(f))


class TestPipelineJob(unittest.TestCase):
    def setUp(self):
        self.config = load_config('test/resources/config.yaml')
        self.feature = {
            'globalid': '{010375CD-1497-4C07-A71D-4DBCF185696F}',
            'url': None,
            'nombre_fichero': None
        }
        self.attach = {
            'rel_globaid': '{4EA25197-A13C-4B87-B145-9C4C2C31F6D8}',
            'content_type': 'image/jpeg',
            'data': load_image('test/resources/test.jpg'),
            'att_name': 'test.jpg'
        }

    def test_returnAttchEmpty_when_pipelineWithExportAndDeleteAttachment(self):
        feature_expected = {
            'globalid': self.feature['globalid'],
            'url': 'https://apd31lv.teide.int/mediastorage/images/010375CD-1497-4C07-A71D-4DBCF185696F.jpg',
            'nombre_fichero': '010375CD-1497-4C07-A71D-4DBCF185696F.jpg'
        }
        pipe = JobPipeline(self.config['mimetypes'], self.feature.keys(), self.attach.keys())
        feature, attach = pipe.run_pipeline(self.feature.copy(), self.attach.copy())

        eq_(feature, feature_expected)
        ok_(path.isfile(path.join('temp/', '010375CD-1497-4C07-A71D-4DBCF185696F.jpg')))
